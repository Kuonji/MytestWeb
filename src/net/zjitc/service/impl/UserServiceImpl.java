package net.zjitc.service.impl;

import net.zjitc.dao.UserDao;
import net.zjitc.domain.User;
import net.zjitc.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Administrator on 2017/2/27.
 */
//增加注解，此对象由sprin管理
@Service
@Transactional
public class UserServiceImpl implements UserService{

    //使用注解自动从spring中取出对象
    @Resource
    private UserDao userDao;

    @Override
    public User findByUsernamePassword(String username, String password) {
        return userDao.findByUsernamePassword(username,password);
    }

    @Override
    public List<User> findAll() {
        return userDao.findAll();
    }
}
