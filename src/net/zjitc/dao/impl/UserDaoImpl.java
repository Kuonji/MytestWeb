package net.zjitc.dao.impl;

import net.zjitc.dao.UserDao;
import net.zjitc.domain.User;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Administrator on 2017/2/27.
 */
@Repository
public class UserDaoImpl implements UserDao{
    @Resource
   private SessionFactory sessionFactory;
  /*  public void init(){
        users[0]= new User(1,"zs1","123456");
        users[1]= new User(2,"zs2","123456");
        users[2]= new User(3,"aaa","123456");
    }*/
  protected Session getSession(){
      Session session = sessionFactory.getCurrentSession();
      return session;
  }
    @Override
    public User findByUsernamePassword(String username, String password) {
        /*初始化几个数据，从里面找登录的用户*/
       /*this.init();*/

       //遍历
    /*    for (User u : users){
            if (u.getUsername().equals(username) && u.getPassword().equals(password)){
                return u;
            }
        }*/
    /*从数据库中查*/   // openSession();

     /*Session session = sessionFactory.openSession();*/
        /*Transaction transaction = session.beginTransaction();*/
        User user = (User) getSession().createQuery(
                "from User u where u.password=? and u.username=?")
                .setParameter(0,password)
                .setParameter(1,username)
                .uniqueResult();
      /*  transaction.commit();
        session.close();*/
        System.out.println("1111");
        return user;
    }

    /*查询所有*/
    @Override
    public List<User> findAll() {
        return getSession().createQuery(
                "from User ")
                .list();
    }
}
