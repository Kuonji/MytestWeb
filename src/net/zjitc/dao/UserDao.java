package net.zjitc.dao;

import net.zjitc.domain.User;

import java.util.List;

/**
 * Created by Administrator on 2017/2/27.
 */
public interface UserDao {
    User findByUsernamePassword(String username, String password);

    List<User> findAll();
}
