import net.zjitc.domain.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Administrator on 2017/3/1.
 */
public class TestHiberate {


    @Test
    public void configTest(){
        //创建configuration实例
        Configuration configuration = new Configuration();
        //自动加载配置 文件
        configuration.configure("hibernate.cfg.xml");

        //构建sessionFactory实例
        SessionFactory sf = configuration.buildSessionFactory();

        /*session实例*/
        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();
        User user = new User();
        user.setUsername("rj1502");
        user.setPassword("123456");

        session.save(user);

        transaction.commit();
        session.close();

    }
    /*查询一个id为3的数据对象*/
    @Test
    public void getTest(){
        //创建configuration实例
        Configuration configuration = new Configuration();
        //自动加载配置 文件
        configuration.configure("hibernate.cfg.xml");

        //构建sessionFactory实例
        SessionFactory sf = configuration.buildSessionFactory();

        /*session实例*/
        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();
       /*User user = (User) session.get(User.class,4);*/
        User user = null;
        /*条件查询*/
        user = (User) session.createQuery(
                "from User a where a.password = ? and a.username=?")
                .setParameter(0,"123456")
                .setParameter(1,"aaa")
                .uniqueResult();

        user.setPassword("654321");
        /*更新对象*/
        // session.update(user);

        transaction.commit();
        session.close();
        System.out.println(user);
    }

    @Test
    public void testBean(){
        ApplicationContext ac = new ClassPathXmlApplicationContext("spring-config.xml");
        SessionFactory sf = (SessionFactory) ac.getBean("sessionFactory");
        System.out.println(sf);
    }

}
