<%@ taglib prefix="s" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/2/27
  Time: 14:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
${message}
<br>
用户名：${username}
<br>
密码：${password}
<br>

<br>
OGNL表达式
<br>
用户名：<s:property value="username"/>
<br>
密码：<s:property value="password"/>
</body>

<%--显示出所有用户的所有信息？？？--%>
<br>
显示出所有用户的所有信息？？？
<%--使用迭代--%>
<%--取map集合中的对象的时候，前面要加#--%>
<br>
<s:iterator value="#userList">
     id = <s:property value="id"/> ....name = <s:property value="username"/>
    ......psw = <s:property value="password"/>
    <br>
</s:iterator>

</html>
